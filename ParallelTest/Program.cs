﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ParallelTest
{
    class Program
    {
        static void Main(string[] args)
        {
          //  string startPath = @"c:\example\start";
          //  string zipPath = @"c:\example\result.zip";
          //  string extractPath = @"c:\example\extract";

           // ZipFile.CreateFromDirectory(startPath, zipPath);

            //ZipFile.ExtractToDirectory(zipPath, extractPath);
        }
        static void Main2(string[] args)
        {
           

            //for (int i = 0; i < 10; i++)
            //{
            //    Factorial();
            //}

            //Parallel.For(1, 10, Factorial2);
            //List<Person> persons = new List<Person>();
            //for (int i = 0; i < 10; i++)
            //{
            //    persons.Add(new Person()
            //    {
            //        Age = i
            //    });
            //}

            //Parallel.ForEach(persons, person => {
            //    PrintAge(person, " Auction ");
            //});

            //List<Task> TaskList = new List<Task>();

            //for (int i = 0; i < 100; i ++)
            //{
            //    TaskList.Add(test(i));
            //}

            Console.WriteLine("Старт");

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            //TaskList.ToArray();

            //List<AveragePrice> arr = new List<AveragePrice>() { new AveragePrice {LotPrice =10, LotsSum = 12}, new AveragePrice { LotPrice = 18, LotsSum = 32 }, new AveragePrice { LotPrice = 60, LotsSum = 52 } };

            //arr.ForEach(x => x.LotPrice = 9999);
            //arr.ForEach(x => Console.WriteLine(x.LotPrice));

            int n;
            bool isNumeric = int.TryParse(null, out n);

            Console.WriteLine("0000123 = " + isNumeric);

            //var sum = arr.Select(x => x.LotsSum).Aggregate((i, j) => i + j);

            //Console.WriteLine($"sum = {sum}");
            //Console.WriteLine(DateTime.Now.ToString("dd.MM.yyyy"));
            //Console.WriteLine(DateTime.Now.ToString("yyMMdd") + "07" + "00001");

            //Console.WriteLine(DateTime.Now.ToString("yyMMddHHmmssFFFF"));

            //Console.WriteLine(Guid.NewGuid().ToString());

            //int num;
            //var builder = new StringBuilder();
            //var str = "181130070001";
            //str = str.Substring(str.Length - 4);

            //var parsed = int.TryParse(str, out num);
            //if (parsed)
            //{
            //    num++;
            //}
            //builder.Append(num.ToString());
            //while (builder.Length <= 3)
            //{
            //    builder.Insert(0, 0);
            //}
            //Console.WriteLine(builder.ToString());

            stopWatch.Stop();
            Console.WriteLine("Время ушло {0}", stopWatch.ElapsedMilliseconds.ToString());
            Console.ReadKey();
        }

        static void Factorial()
        {
            Console.WriteLine("Выполняется задача {0}", Task.CurrentId);
            Thread.Sleep(1000);
        }

        static void Factorial2(int x)
        {
            Console.WriteLine("Выполняется задача {0}", Task.CurrentId);
            Thread.Sleep(1000);
        }

        static void PrintAge(Person person, string auction)
        {
            Console.WriteLine($"Выполняется задача {Task.CurrentId}" + $" person.Age =  {person.Age}" + auction);
            Thread.Sleep(1000);
        }

        public static async Task test(int i)
        {
            await Task.Run(() =>
            {
                Console.WriteLine(Task.CurrentId);
                Thread.Sleep(1000);
            });
           
        }
    }


    public class Person
    {
        public int Age { get; set; }
    }
}
