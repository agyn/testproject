const {exec} = require('child_process');

const projects = [
    {env: 'IDENTITY', dir: 'RSC.IdentityServer', out: 'identity'},
    {env: 'FILESVC', dir: 'RSC.FileStorage', out: 'file'},
    {env: 'PORTAL', dir: 'Pet.Portal.Api', out: 'portal'},
	{env: 'ADMIN', dir: 'Pet.Admin.Api', out: 'admin'},
	{env: 'COMMON', dir: 'Pet.Common.Api', out: 'common'},
	{env: 'INSTRUMENT', dir: 'Pet.Instrument.Scheduler', out: 'instrument'},
	{env: 'TEST', dir: 'Test', out: 'test'}
];

function execute(command) {
    return new Promise((resolve, reject) => {
        exec(command, (error, stdout, stderr) => {
            let log = command + '\n' + stdout + '\n' + stderr;
            if (error) {
                reject(log + '\n' + error);
            }
            resolve(log);
        });
    });
}

function publish(project) {
    let res = Promise.resolve('');
    let log = '';
    return res
        // .then(res => {
            // log += res;
            // console.log(res);
            // return execute(`del /S /F /Q C:\\petPublish\\temp\\api\\${project.out}\\*`)
                // .catch(err => Promise.resolve(err));
        // })
        .then(res => {
            log += res;
            console.log(res);
            return execute(`dotnet publish ${project.dir} -o C:\\petPublish\\temp\\api\\${project.out}`)
        })
        // .then(res => {
            // log += res;
            // console.log(res);
            // return execute(`C:\\Windows\\System32\\inetsrv\\appcmd stop apppool /apppool.name:"Pet.${project.out}"`);
        // })
        // .then(res => {
            // log += res;
            // console.log(res);
            // return execute(`del /S /F /Q C:\\petPublish\\api\\${project.out}\\*`)
                // .catch(err => Promise.resolve(err));
        // })
        // .then(res => {
            // log += res;
            // console.log(res);
            // return execute(`xcopy /e /i /h C:\\petPublish\\temp\\api\\${project.out}\\* C:\\petPublish\\api\\${project.out}`)
        // })
        // .then(res => {
            // log += res;
            // console.log(res);
            // return execute(`C:\\Windows\\System32\\inetsrv\\appcmd start apppool /apppool.name:"Pet.${project.out}"`);
        // })
        .then(res => Promise.resolve(log + res))
        .catch(err => Promise.reject(log + err));
}

let isError = [];
let log = '';
console.log('Building solution!');

var path = require('path');
var userName = process.env['USERPROFILE'].split(path.sep)[2];
var loginId = path.join("domainName",userName);
console.log(loginId);

var userName2 = process.env['USERNAME']
console.log(userName2);



execute('cd ParallelTest && dotnet build')
    .then(res => {
        log += res;
    })
    .then(res => {
        console.log(log + res);
        console.log('Solution building is completed');
        log = '';
        let promises = [];

        projects.forEach(project => {
            if (process.env.ALL === 'true' || process.env[project.env] === 'true') {
                console.log(`Publishing ${project.env}`);
                promises.push(
                    publish(project)
                        .then(res => {
                            console.log(res);
                            console.log(`Project ${project.dir} published`);
                            return Promise.resolve('');
                        })
                        .catch(err => {
                            isError.push(err);
                            return Promise.resolve('');
                        }));
            }
        });

        return Promise.all(promises);
    })
    .then(res => {
        if (isError.length > 0) {
            isError.forEach(err => console.error(err));
            process.exit(1);
        }
		if(process.env.MIGRATION === 'true'){
			console.log('Apply migrations..');
			return execute('set ASPNETCORE_ENVIRONMENT=Production && dotnet ef database update --project Pet.Shared.Data --startup-project RSC.IdentityServer --configuration Production')
             .then(res => {
                 console.log(res);
                 console.log('All ok!');
             });
		}
      
    })
    .catch(err => {
        console.error(log + err);
        process.exit(1);
    });